\name{within.extent}
\alias{within.extent}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Test if a coordinate or extent lies within another extent
}
\description{
Test whether a given coordinate pair or a given extent falls within another extent. A tolerance can also be specified.
}
\usage{
within.extent(coords, ext, tolerance = NULL)
}
\arguments{
  \item{coords}{
The coordinate pair (x,y) or an extent vector c(xmin, xmax, ymin, ymax) to compare with another extent}
  \item{ext}{
Extent to be compared to coords
}
  \item{tolerance}{
Optional. Allow a tolerance which will be applied to both spatial dimensions of the extent.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
TRUE (falls within the extent) or FALSE (does not fall within the extent)
}
\references{
}
\author{
Ben DeVries
}
\note{
Note that overlapping extent objects will not return a TRUE value. The coords object must fall within the ext object for a TRUE to be returned.
}

\seealso{
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (coords, ext, tolerance = NULL) 
{
    if (is.null(tolerance)) 
        tolerance <- 0
    coords <- as.vector(coords)
    if (length(coords) != 2 & length(coords) != 4) 
        stop("coords must be of length 2 (coord pair) or 4 (extent vector)")
    if (length(coords) == 2) 
        test <- coords[1] >= xmin(ext) - tolerance & coords[1] <= 
            xmax(ext) + tolerance & coords[2] >= ymin(ext) - 
            tolerance & coords[2] <= ymax(ext) + tolerance
    if (length(coords) == 4) 
        test <- coords[1] >= xmin(ext) - tolerance & coords[1] <= 
            xmax(ext) & coords[2] >= xmin(ext) & coords[2] <= 
            xmax(ext) + tolerance & coords[3] >= ymin(ext) - 
            tolerance & coords[3] <= ymax(ext) & coords[4] >= 
            ymin(ext) & coords[4] <= ymax(ext) + tolerance
    return(test)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
