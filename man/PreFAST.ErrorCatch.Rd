\name{PreFAST.ErrorCatch}
\alias{PreFAST.ErrorCatch}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Error catch
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
PreFAST.ErrorCatch(ledaps = FALSE, datadir, NDVIdir, FMask = TRUE, flag = 0, aoi, StackDir = NDVIdir, StackName = "NDVI_stack.grd", mc.cores = 1)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{ledaps}{
%%     ~~Describe \code{ledaps} here~~
}
  \item{datadir}{
%%     ~~Describe \code{datadir} here~~
}
  \item{NDVIdir}{
%%     ~~Describe \code{NDVIdir} here~~
}
  \item{FMask}{
%%     ~~Describe \code{FMask} here~~
}
  \item{flag}{
%%     ~~Describe \code{flag} here~~
}
  \item{aoi}{
%%     ~~Describe \code{aoi} here~~
}
  \item{StackDir}{
%%     ~~Describe \code{StackDir} here~~
}
  \item{StackName}{
%%     ~~Describe \code{StackName} here~~
}
  \item{mc.cores}{
%%     ~~Describe \code{mc.cores} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (ledaps = FALSE, datadir, NDVIdir, FMask = TRUE, flag = 0, 
    aoi, StackDir = NDVIdir, StackName = "NDVI_stack.grd", mc.cores = 1) 
{
    require(raster)
    require(parallel)
    foldlist = list.files(datadir, pattern = glob2rx("L*"))
    sceneList = character(length(foldlist))
    process <- function(foldName) {
        dir = paste(datadir, foldName, "/", sep = "")
        if (ledaps == TRUE) {
            RunLedaps(dir = dir)
        }
        NameDVI = NDVMask(dir = dir, NDVIdir = NDVIdir, FMask = FMask, 
            flag = flag, aoi = aoi)
        return(NameDVI)
    }
    catch <- function(foldName) {
        return(tryCatch({
            process(foldName)
        }, error = function(e) {
            return(sprintf("Scene \%s cannot be processed", foldName))
        }))
    }
    outList = mclapply(foldlist, catch, mc.cores = mc.cores)
    TiffList = unlist(outList)
    NDVStack(list = TiffList, StackName = StackName, StackDir = StackDir)
    MakeStackTimeVector(stack = paste(StackDir, StackName, sep = ""), 
        filename = paste(StackDir, StackName, ".Time", sep = ""))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
