\name{RunLedaps}
\alias{RunLedaps}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Processes ledaps
}
\description{
Pre-processes Landsat data using ledaps
}
\usage{
RunLedaps(dir)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{dir}{
Dir where raw scenes are located
}
}
\details{

}
\value{
None
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Jose, Loïc
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ledaps}
\keyword{Landsat}% __ONLY ONE__ keyword per line
