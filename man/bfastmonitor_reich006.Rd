\name{bfastmonitor_reich006}
\alias{bfastmonitor_reich006}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Run Bfast Monitor on a Raster Brick
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
run_bfm(data, history = "all", monperiod = c(), monend = "full", formula = response ~ trend + harmon, order = 1, filename = "", overwrite = FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{data}{
input raster time series brick
}
  \item{history}{
stable history period. If history=="all" (default condition), all available data is used. Otherwise, it should be in the form c(year, julian day), representing the start of the stable history period
}
  \item{monperiod}{
The start of the monitoring period in the form c(year, julian day)
}
  \item{monend}{
Optional. The end of the monitoring period in the form c(year, julian day). The input raster brick will be trimmed after this date and monitoring will stop there. By default monend=="full", in which case the full time series after monperiod is monitored
}
  \item{formula}{
Model to use for the stable history period. See package::bfast for more information
}
  \item{order}{
Order of the harmonic component
}
  \item{sceneID}{
Optional. Character vector with Landsat scene ID's. If supplied, these must be in the standard Landsat format (e.g. "LE71700552001036SGS00" or "LT51700551986035XXX02"). If omitted (as in the default), these scene ID's must be assigned as layer names in the input raster brick.
}
  \item{filename}{
Optional. Write result to file (raster brick)
}
  \item{overwrite}{
Overwrite existing file?
}
}
\details{
}
\value{
Raster brick with 3 components:
  1) Breakpoint timing
  2) Change Magnitude
  3) History period (years)
}
\references{
Verbesselt, J., Zeileis, A., & Herold, M. (2012). Near real-time disturbance detection using satellite image time series. Remote Sensing of Environment, 123, 98–108
}
\author{
Ben DeVries, Jan Verbesselt
}
\note{
}

\seealso{
}
\examples{
library(raster)
library(bfast)

# load in input data
data(chomecha)

# run bfm with a monitoring period starting at the beginning of 2005 
# and a stable history period starting at the beginning of 1999
bfm05 <- run_bfm(chomecha, monperiod=c(2005, 1), history=c(1999, 1))
plot(bfm05, 1) # view breakpoint timing

# run bfm with monitoring period 2005-2006 (ie. trim the time series)
bfm0506 <- run_bfm(chomecha, monperiod=c(2005, 1), history=c(1999, 1), monend=c(2006, 1))
plot(bfm0506, 1)
}

