run_bfm_reich006_cloud_mc <- function(data, history="all", monperiod=c(), monend="full", formula=response~trend+harmon, order=1, type = "OLS-MOSUM", 
                                      sceneID=NULL, filename="", overwrite=FALSE,mc.cores=mc.cores, cloud=TRUE,h=0.25,hdyn = FALSE,history_obs_min=2)
  #To run it on SAR TS input needs to be positive (e.g. hv * -1 instead of hv)
{
  
  if(is.null(monperiod)) 
    stop("missing value for monperiod (ie. start of the monitoring period)")
  if(monend[1]!="full" & !is.numeric(monend)) 
    stop("monend should either be \'full\' or numeric of length=2 (last date of monitoring period)")
  #if(monend<monperiod) 
  #stop("monend must be greater than monperiod")
  
  b <- data
  
  # get dates and years from sceneID vector or calculate dates and years from layer names
  if(is.null(sceneID))
    sceneID <- row.names(get.sceneinfo(names(b)))
  dates <- get.sceneinfo(sceneID)$date
  
  # trim time series if monend!="full"
  if(monend[1]!="full"){
    end.date <- as.Date(paste(monend[1], monend[2], sep="-"), format="%Y-%j")
    b <- dropLayer(b, which(dates > end.date))
    
    # redefine dates based on trimmed raster brick
    sceneID <- sceneID[which(dates <= end.date)]
    dates <-get.sceneinfo(sceneID)$date
  }
  
  # declare bfm helper functions
  # TODO: rewrite ybfastmonitor to include bfm.pixel()?
  ybfastmonitor_new <- function(x, dates) {
    vi <- bfastts(x, dates, type = c("irregular"))
    #vi[2] <- vi[1]
    #reich006: vi can also be <= 0 for SAR ts
    #vi[vi <= 0] <- NA
    bfm <- bfastmonitor_reich006(formula = formula, order = order, 
                                 data = vi, start = monperiod, history = history,hdyn=hdyn,history_obs_min=history_obs_min)
    if(cloud==TRUE){
      # if break detected in bfm check if it is valid
      if(!is.na(bfm$breakpoint)){
        vi2 <- vi
        #set break na
        vi2[time(vi2) == bfm$breakpoint] <- NA
        bfm2 <- bfastmonitor_reich006(formula = formula, order = order, 
                                      data = vi2, start = monperiod, history = history,hdyn=hdyn,history_obs_min=history_obs_min)
        #if break detected in bfm2
        if(!is.na(bfm2$breakpoint)){
          #index of detected break bfm1
          bfmi <-  which(time(vi) == bfm$breakpoint)
          #index of detected break bfm2
          bfm2i <- which(time(vi) == bfm2$breakpoint)
          #index of detected break bfm1 in sequence (1,2,3,4 ...) of TS elements with value
          bfm1is <- which(bfmi==which(!is.na(vi)))
          #index of detected break bfm2 in sequence (1,2,3,4 ...) of TS elements with value
          bfm2is <- which(bfm2i==which(!is.na(vi)))
          if(bfm1is+1==bfm2is){
            #print(sprintf("bfm$breakpoint <- bfm$breakpoint"))
            bfm$breakpoint <- bfm$breakpoint
          } else {
            bfm$breakpoint <- bfm2$breakpoint
            #print(sprintf("bfm$breakpoint <- bfm2$breakpoint"))
          }
        } else {
          bfm$breakpoint <- NA
          #print(sprintf("bfm$breakpoint <- NA"))
        }
      }
    }
    #nr of observaitons in history period
    if(length(bfm$history[])>1){lhistory <- length(which((bfm$tspp$time >= bfm$history[1] & bfm$tspp$time <= bfm$history[2]) == TRUE) )
    } else {lhistory <- 0}
    print(lhistory)
    return(cbind(bfm$breakpoint, bfm$magnitude, (bfm$history[2] - 
                                                   bfm$history[1]),lhistory))
  }
  fun <- function(y) {
    percNA <- apply(y, 1, FUN = function(x) (sum(is.na(x))/length(x)))
    i <- ((percNA < 1)) # logical vector corresponding to pixel ts
    res <- matrix(NA, length(i), 4)
    
    # do not apply bfm if there are only NA's in the pixel ts
    if (sum(i) > 0) {
      print("indside")
      res[i, ] <- t(apply(y[i, ], 1, ybfastmonitor_new, dates))
    }
    res
  }
  
  if(filename!="")
    x <- mc.calc(b, fun=fun, filename=filename, overwrite=overwrite, mc.cores=mc.cores)
  else
    x <- mc.calc(b, fun=fun, mc.cores=mc.cores)
  
  return(x)
}