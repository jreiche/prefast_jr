calcBandStack <- function (dir, bands=c(1,2,3,4,5,7), cloudmask = NA, flag=0, aoi=NA) 
# reads Ledaps output hdf files and stacks desired bands for single Landsat files
# (optional) aoi file for cropping
# (optional) use FMASK or Ledaps cloud mask
    
# During scripting, the ledaps cloud mask hdf file was not read correctly! Should be fixed!
    
{
  require(raster)
  require(MODIS)
  
  # cropping function
  cropS <- function(aoi, raster) {
    require(maptools)
    aoi = readShapePoly(aoi)
    c = crop(raster, aoi)
    return(c)
  }
  
  # apply FMask cloud mask
  if (!is.na(cloudmask)){
      if (cloudmask == "FMask") {
          maskF = raster(list.files(dir, pattern = glob2rx("*MTLFmask")))
      }
      # apply Ledaps cloud mask. At the time of programming there were reading problems
      if (cloudmask == "Ledaps") {
        hdfMask = list.files(pattern = glob2rx("lndcsm*.hdf"))
        maskF = raster(hdfMask)
      }
      # crop cloud mask with aoi
      if (!is.na(aoi)) {
          mask <- cropS(aoi = aoi, raster = maskF)
          } else {
              mask <- maskF
          }
  }
  
  # read hdf file
  hdf = list.files(pattern = glob2rx("lndsr*.hdf"))
  SDS = getSds(paste(dir, hdf, sep = ""))
  # extract band    
  for(r in 1:length(bands)){
      bandF <- raster(SDS$SDS4gdal[bands[r]])
      bandF <- bandF * 10000
      # crop bands with aoi
      if (!is.na(aoi)) {
          band = cropS(aoi = aoi, raster = bandF)
      } else {
          band <- bandF
      }
      # flag bands if cloud mask was applied
      if (!is.na(cloudmask)) {
          band[mask != flag] <- NA
      }
      # stack bands
      if(r==1){
          b1 <- stack(band)
      } else {
          b1 <- addLayer(b1,band)
      }
      rm(bandF)
      rm(band) 
  }
  return(b1)
}


